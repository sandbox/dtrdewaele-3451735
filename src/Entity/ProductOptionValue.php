<?php

declare(strict_types=1);

namespace Drupal\commerce_product_options\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the product option value entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_product_option_value",
 *   label = @Translation("Product option value"),
 *   label_collection = @Translation("Product option values"),
 *   label_singular = @Translation("product option value"),
 *   label_plural = @Translation("product option values"),
 *   label_count = @PluralTranslation(
 *     singular = "@count product option values",
 *     plural = "@count product option values",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "storage" = "Drupal\commerce_product_options\ProductOptionValueStorage",
 *     "access" = "Drupal\commerce_product_options\ProductOptionValueAccessControlHandler",
 *   },
 *   base_table = "commerce_product_option_value",
 *   data_table = "commerce_product_option_value_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer commerce_product_option_value",
 *   entity_keys = {
 *     "id" = "id",
*      "bundle" = "option",
 *     "langcode" = "langcode",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   bundle_entity_type = "commerce_product_option",
 *   field_ui_base_route = "entity.commerce_product_option_value.edit_form",
 * )
 */
final class ProductOptionValue extends ContentEntityBase implements ProductOptionValueInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return (int) $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight(int $weight): void {
    $this->set('weight', $weight);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['option'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Product option'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setTranslatable(TRUE)
      ->setDescription(t('The weight of this product option value in relation to other values.'))
      ->setDefaultValue(0);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the product option value was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the product option value was last edited.'));

    return $fields;
  }

}
