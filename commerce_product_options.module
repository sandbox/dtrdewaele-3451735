<?php

/**
 * @file
 * Hook implementations for Commerce product options module.
 */

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Implements hook_entity_operation().
 */
function commerce_product_options_entity_operation(EntityInterface $entity) {
  if (!$entity instanceof ProductVariationInterface) {
    return [];
  }

  $operations = [];
  $commerceProduct = $entity->getProduct();

  if ($commerceProduct) {
    $operations['product_options'] = [
      'title' => t('Product Options'),
      'weight' => 100,
      'url' => Url::fromRoute('commerce_product_options.variation_configurator', [
        'commerce_product' => $commerceProduct->id(),
        'commerce_product_variation' => $entity->id(),
      ]),
    ];
  }

  return $operations;
}
