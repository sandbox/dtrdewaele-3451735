<?php

namespace Drupal\commerce_product_options\Plugin\Field\FieldFormatter;

use Drupal\commerce_product_options\ProductOptionValueStorageInterface;
use Drupal\commerce_product_options\Service\CommerceProductOptionsService;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'product_options_formatter' formatter.
 */
#[FieldFormatter(
  id: "commerce_product_options_formatter",
  label: new TranslatableMarkup("Options as item list"),
  field_types: [
    "commerce_product_options",
  ]
)]
class CommerceProductOptionsFormatter extends FormatterBase {

  /**
   * The product option value storage.
   *
   * @var \Drupal\commerce_product_options\ProductOptionValueStorageInterface
   */
  protected ProductOptionValueStorageInterface $commerceProductOptionValueStorage;

  /**
   * Creates a new CommerceProductOptionsFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\commerce_product_options\Service\CommerceProductOptionsService $commerceProductOptionsService
   *   The commerce product options service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    protected readonly CommerceProductOptionsService $commerceProductOptionsService,
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): CommerceProductOptionsFormatter {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('commerce_product_options.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $optionListItems = [];

    foreach ($items as $delta => $item) {
      $value = unserialize($item->get('value')->getValue(), [
        "allowed_classes" => FALSE,
      ]);

      foreach ($value as $optionId => $optionValue) {
        $productOption = $this->commerceProductOptionsService->getLoadedProductOption($optionId);
        $productOptionValue = $this->commerceProductOptionsService->getLoadedProductOptionValue($optionValue);

        if (!$productOption || !$productOptionValue) {
          continue;
        }

        $optionListItems[] = new TranslatableMarkup('<strong>@label</strong>: @value', [
          '@label' => $productOption->label(),
          '@value' => $productOptionValue->label(),
        ]);
      }

      $elements[$delta][] = [
        '#theme' => 'item_list',
        '#items' => $optionListItems,
      ];
    }

    return $elements;
  }
}
