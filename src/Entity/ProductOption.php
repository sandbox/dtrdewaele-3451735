<?php

declare(strict_types=1);

namespace Drupal\commerce_product_options\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the product option entity type.
 *
 * @ConfigEntityType(
 *   id = "commerce_product_option",
 *   label = @Translation("Product option"),
 *   label_collection = @Translation("Product options"),
 *   label_singular = @Translation("product option"),
 *   label_plural = @Translation("product options"),
 *   label_count = @PluralTranslation(
 *     singular = "@count product option",
 *     plural = "@count product options",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\commerce_product_options\ProductOptionListBuilder",
 *     "storage" = "Drupal\commerce_product_options\ProductOptionStorage",
 *     "form" = {
 *       "add" = "Drupal\commerce_product_options\Form\ProductOptionForm",
 *       "edit" = "Drupal\commerce_product_options\Form\ProductOptionForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "commerce_product_option",
 *   admin_permission = "administer commerce_product_option",
 *   links = {
 *     "collection" = "/admin/structure/commerce-product-option",
 *     "add-form" = "/admin/structure/commerce-product-option/add",
 *     "edit-form" = "/admin/structure/commerce-product-option/{commerce_product_option}",
 *     "delete-form" = "/admin/structure/commerce-product-option/{commerce_product_option}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "product_variation_types" = "product_variation_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "product_variation_types",
 *   },
 * )
 */
final class ProductOption extends ConfigEntityBase implements ProductOptionInterface {

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The example label.
   */
  protected string $label;

  /**
   * The example description.
   */
  protected string $description;

  /**
   * The product variation types where this product option is enabled.
   *
   * @var array
   */
  protected array $productVariationTypes;

  /**
   * {@inheritdoc}
   */
  public function getValues(): array {
    /** @var \Drupal\commerce_product_options\ProductOptionValueStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage('commerce_product_option_value');
    $values = $storage->loadMultipleByOption($this->id());
    // Make sure that the values are returned in the attribute language.
    $langcode = $this->language()->getId();
    foreach ($values as $index => $value) {
      if ($value->hasTranslation($langcode)) {
        $values[$index] = $value->getTranslation($langcode);
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption(): ProductOptionInterface {
    /** @var \Drupal\commerce_product_options\Entity\ProductOptionInterface $productOption */
    $productOption = $this->entityTypeManager()
      ->getStorage('commerce_product_option')
      ->load($this->id());

    return $productOption;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariationTypes(): array {
    return $this->get('product_variation_types');
  }

}
