<?php

namespace Drupal\commerce_product_options\OrderProcessor;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_product_options\Service\CommerceProductOptionsService;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Order processor for product options.
 */
class CommerceProductOptionsOrderProcessor implements OrderProcessorInterface {

  /**
   * Creates a new CommerceProductOptionsOrderProcessor object.
   *
   * @param \Drupal\commerce_product_options\Service\CommerceProductOptionsService $commerceProductOptionsService
   *   The commerce product options service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    readonly protected CommerceProductOptionsService $commerceProductOptionsService,
    readonly protected EntityFieldManagerInterface $entityFieldManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order): void {
    foreach ($order->getItems() as $orderItem) {
      $fieldName = $this->getProductOptionsField($orderItem);

      if ($fieldName
        && $orderItem->hasField($fieldName)
        && !$orderItem->get($fieldName)->isEmpty()) {
        $productOptions = $orderItem->get($fieldName)->getString();
        $productOptions = unserialize($productOptions, ["allowed_classes" => FALSE]);

        $priceWithOptions = $this->commerceProductOptionsService->calculatePrice(
          $orderItem->getPurchasedEntity(),
          $productOptions);

        $orderItem->setUnitPrice($priceWithOptions);
      }
    }
  }

  /**
   * Get the field name for the product options field.
   *
   * For now we loop over all fields and return the first one that is of type
   * commerce_product_options. This is not ideal and should be improved.
   *
   * @param $orderItem
   *   The order item.
   *
   * @return string|null
   */
  private function getProductOptionsField($orderItem): ?string {
    $fields = $this->entityFieldManager->getFieldDefinitions(
      $orderItem->getEntityTypeId(),
      $orderItem->bundle()
    );

    foreach ($fields as $fieldName => $fieldConfig) {
      if ($fieldConfig->getType() === 'commerce_product_options') {
        return $fieldName;
      }
    }

    return NULL;
  }

}
