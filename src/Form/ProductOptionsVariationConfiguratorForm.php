<?php

namespace Drupal\commerce_product_options\Form;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_options\Entity\ProductOptionValue;
use Drupal\commerce_product_options\Service\CommerceProductOptionsServiceInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the product options variation configurator form.
 *
 * // @todo Fix saving issues with first product option.
 */
class ProductOptionsVariationConfiguratorForm extends FormBase {

  /**
   * Constructs a new ProductOptionsVariationConfiguratorForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\commerce_product_options\Service\CommerceProductOptionsServiceInterface $commerceProductOptionsService
   *   The commerce product options service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   The logger channel.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected Connection $database,
    protected CommerceProductOptionsServiceInterface $commerceProductOptionsService,
    protected LoggerChannelInterface $loggerChannel,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ProductOptionsVariationConfiguratorForm {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('commerce_product_options.service'),
      $container->get('logger.channel.commerce_product_options'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'commerce_product_options_variation_configurator_form';
  }

  /**
   * {@inheritdoc}
   *
   * // @todo handle double add new.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ProductInterface $commerce_product = NULL, ProductVariationInterface $commerce_product_variation = NULL): array {
    /** @var \Drupal\commerce_product_options\ProductOptionStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('commerce_product_option');
    $options = $storage->loadByVariationType($commerce_product_variation->bundle());

    // If triggering element is set, use the user input to get the default
    // values.
    if ($triggeringElement = $form_state->getTriggeringElement()) {
      $defaultValues = $form_state->getUserInput();
      // @todo creation function for repeated code.
      $defaultValues = array_filter($defaultValues, static function ($key) {
        return str_contains($key, 'option_');
      }, ARRAY_FILTER_USE_KEY);

      // Handle the remove action.
      if ($triggeringElement['#action'] === 'remove') {
        $optionId = $triggeringElement['#option_id'];
        $value_index = $triggeringElement['#value_index'];
        unset($defaultValues['option_' . $optionId]['options'][$value_index]);
      }

      // Handle add new action.
      if ($triggeringElement['#action'] === 'add') {
        $optionId = $triggeringElement['#option_id'];
        $value_map = $form_state->get('value_map');
        $value_map[] = '_new';
        $form_state->set('value_map', $value_map);
        $defaultValues['option_' . $optionId]['options'][] = [[]];
      }
    }
    else {
      // Get the default values from the database if no triggering element is
      // set.
      $defaultValues = $this->commerceProductOptionsService->getVariationOptions($commerce_product_variation);
    }

    $form['#tree'] = TRUE;

    if ($options) {
      /** @var \Drupal\commerce_product_options\Entity\ProductOptionInterface $option */
      foreach ($options as $option) {
        $optionId = $option->id();
        $wrapperId = Html::getUniqueId('product-option-' . $optionId . '-ajax-wrapper');

        $defaultValuesIndex = 'option_' . $optionId;
        $optionDefaultValues = (isset($defaultValues[$defaultValuesIndex]) && is_array($defaultValues[$defaultValuesIndex])
          ? $defaultValues[$defaultValuesIndex]['options']
          : []);
        if (is_null($optionDefaultValues)) {
          $optionDefaultValues = [];
        }

        $form['option_' . $optionId] = [
          '#type' => 'details',
          '#title' => $option->label(),
        ];

        $form['option_' . $optionId]['options'] = [
          '#type' => 'table',
          '#header' => [
            $this->t('Option value'),
            $this->t('Price'),
            $this->t('Weight'),
            $this->t('Operations'),
          ],
          '#empty' => $this->t('No product options added yet.'),
          '#prefix' => '<div id="' . $wrapperId . '">',
          '#suffix' => '</div>',
          '#tabledrag' => [
            [
              'action' => 'order',
              'relationship' => 'sibling',
              'group' => 'option-order-weight',
            ],
          ],
        ];

        foreach ($optionDefaultValues as $optionDefaultValueKey => $optionDefaultValue) {
          $productOptionValueId = $optionDefaultValue['value'] ?? 0;

          if (!is_int($productOptionValueId)) {
            // Get all values between the parenthesis and convert to an integer
            // from $prodcutOPtionsValueId.
            preg_match_all('!\d+!', $productOptionValueId, $matches);
            $productOptionValueId = (int) $matches[0][0];
          }
          $productOption = ProductOptionValue::load($productOptionValueId);

          $form['option_' . $optionId]['options'][$optionDefaultValueKey]['#attributes']['class'][] = 'draggable';
          $form['option_' . $optionId]['options'][$optionDefaultValueKey]['#weight'] = $optionDefaultValue['weight'] ?? 0;

          $form['option_' . $optionId]['options'][$optionDefaultValueKey]['value'] = [
            '#type' => 'entity_autocomplete',
            '#title' => $this->t('Option value'),
            '#description' => $this->t('Use autocomplete function to search for an option value. New values will be saved automatically.'),
            '#title_display' => 'invisible',
            '#target_type' => 'commerce_product_option_value',
            '#selection_handler' => 'default:commerce_product_option_value',
            '#selection_settings' => [
              'target_bundles' => [
                $optionId => $optionId,
              ],
              'match_operator' => 'CONTAINS',
              'match_limit' => 10,
            ],
            '#default_value' => $productOption,
            '#required' => TRUE,
          ];

          $form['option_' . $optionId]['options'][$optionDefaultValueKey]['price'] = [
            '#type' => 'commerce_price',
            '#title' => $this->t('Price'),
            '#default_value' => [
              'number' => $optionDefaultValue['price']['number'] ?? NULL,
              'currency_code' => $optionDefaultValue['price']['currency_code'] ?? NULL,
            ],
            '#title_display' => 'invisible',
            '#required' => TRUE,
          ];

          // Weight col.
          $form['option_' . $optionId]['options'][$optionDefaultValueKey]['weight'] = [
            '#type' => 'weight',
            '#title' => $this->t('Weight'),
            '#title_display' => 'invisible',
            '#default_value' => $optionDefaultValue['weight'] ?? 0,
            '#attributes' => ['class' => ['option-order-weight']],
          ];

          $form['option_' . $optionId]['options'][$optionDefaultValueKey]['remove'] = [
            '#type' => 'submit',
            '#name' => 'remove_' . $optionId . '_' . $optionDefaultValueKey,
            '#value' => $this->t('Remove'),
            '#limit_validation_errors' => [],
            '#submit' => ['::removeValueSubmit'],
            '#value_index' => $optionDefaultValueKey,
            '#option_id' => $optionId,
            '#action' => 'remove',
            '#ajax' => [
              'callback' => '::valuesAjax',
              'wrapper' => $wrapperId,
            ],
            // @todo check access for removing.
            // '#access' => $remove_access,
          ];
        }

        $form['option_' . $optionId]['_add_new'] = [
          '#tree' => FALSE,
        ];
        $form['option_' . $optionId]['_add_new_' . $optionId]['entity'] = [
          '#type' => 'container',
          '#wrapper_attributes' => ['colspan' => 2],
        ];
        $form['option_' . $optionId]['_add_new']['entity']['add_value'] = [
          '#type' => 'submit',
          // @todo check if we really need to have a unique value.
          '#value' => $this->t('Add value ' . $optionId),
          '#submit' => ['::addValueSubmit'],
          '#limit_validation_errors' => [],
          '#option_id' => $optionId,
          '#action' => 'add',
          '#ajax' => [
            'callback' => '::valuesAjax',
            'wrapper' => $wrapperId,
          ],
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * Ajax callback for value operations.
   */
  public function valuesAjax(array $form, FormStateInterface $form_state) {
    $triggerElement = $form_state->getTriggeringElement();
    $option_id = $triggerElement['#option_id'];
    return $form['option_' . $option_id]['options'];
  }

  /**
   * Submit callback for removing a value.
   */
  public function removeValueSubmit(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Submit callback for adding a new value.
   */
  public function addValueSubmit(array $form, FormStateInterface $form_state) {
    $value_map = (array) $form_state->get('value_map');
    $value_map[] = '_new';
    $form_state->set('value_map', $value_map);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $optionValues = array_filter($form_state->getValues(), static function ($key) {
      return str_contains($key, 'option_');
    }, ARRAY_FILTER_USE_KEY);

    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation */
    $productVariation = $this->getRouteMatch()->getParameter('commerce_product_variation');

    // @todo this has to be refactored and find a better way to remove the deleted options.
    $this->database->delete('commerce_product_variations_product_options')
      ->condition('variation_id', $productVariation->id())
      ->execute();

    foreach ($optionValues as $optionKey => $optionValue) {
      if (!is_array($optionValue['options'])) {
        continue;
      }

      foreach ($optionValue['options'] as $optionValueInformation) {
        if ($optionValueInformation['value']) {
          $optionId = str_replace('option_', '', $optionKey);
          try {
            $this->database
              ->merge('commerce_product_variations_product_options')
              ->condition('variation_id', $productVariation->id())
              ->condition('option_id', $optionId)
              ->condition('option_value_id', $optionValueInformation['value'])
              ->fields([
                'variation_id' => $productVariation->id(),
                'option_id' => $optionId,
                'option_value_id' => $optionValueInformation['value'],
                'price__number' => $optionValueInformation['price']['number'],
                'price__currency_code' => $optionValueInformation['price']['currency_code'],
                'weight' => $optionValueInformation['weight'],
              ])
              ->execute();
          }
          catch (\Exception $e) {
            $this->loggerChannel->error($this->t('Error saving product options for variation @variation_id with message @message', [
              '@variation_id' => $productVariation->id(),
              '@message' => $e->getMessage(),
            ]));
          }
        }
      }
    }
  }

}
