<?php

namespace Drupal\commerce_product_options;

/**
 * Defines an interface for product option storage.
 */
interface ProductOptionStorageInterface {

  /**
   * Loads product options by variation type.
   *
   * @param string $variationTypeId
   *   The variation type ID.
   *
   * @return array
   *   An array of product options.
   */
  public function loadByVariationType(string $variationTypeId): array;

}
