<?php

namespace Drupal\commerce_product_options;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for product option value storage.
 */
interface ProductOptionValueStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads multiple product option values by the given option ID.
   *
   * @param string $optionId
   *   The option ID.
   *
   * @return array
   *   The product option values.
   */
  public function loadMultipleByOption(string $optionId): array;

}
