<?php

namespace Drupal\commerce_product_options\Service;

use Drupal\commerce_product_options\Entity\ProductOptionInterface;
use Drupal\commerce_product_options\Entity\ProductOptionValueInterface;
use Drupal\commerce_product_options\ProductOptionStorageInterface;
use Drupal\commerce_product_options\ProductOptionValueStorageInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * The commerce product options service.
 */
final class CommerceProductOptionsService implements CommerceProductOptionsServiceInterface {

  /**
   * @var \Drupal\commerce_product_options\ProductOptionStorageInterface
   */
  protected readonly ProductOptionStorageInterface $productOptionStorage;

  protected readonly ProductOptionValueStorageInterface $productOptionValueStorage;

  /**
   * Constructs a new CommerceProductOptionsService object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   The logger channel.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    protected readonly Connection $database,
    protected readonly LoggerChannelInterface $loggerChannel,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    /** @var \Drupal\commerce_product_options\ProductOptionStorageInterface $productOptionStorage */
    $productOptionStorage = $this->entityTypeManager->getStorage('commerce_product_option');
    $this->productOptionStorage = $productOptionStorage;

    /** @var \Drupal\commerce_product_options\ProductOptionValueStorageInterface $productOptionValueStorage */
    $productOptionValueStorage = $this->entityTypeManager->getStorage('commerce_product_option_value');
    $this->productOptionValueStorage = $productOptionValueStorage;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariationOptions(ProductVariationInterface $productVariation): array {
    $variationOptions = [];

    try {
      $records = $this->database->select('commerce_product_variations_product_options', 'cpov')
        ->fields('cpov')
        ->condition('cpov.variation_id', $productVariation->id())
        ->orderBy('cpov.weight')
        ->execute()
        ->fetchAll();

      foreach ($records as $row) {
        $variationOptions['option_' . $row->option_id]['options'][] = [
          'value' => (int) $row->option_value_id,
          'price' => [
            'number' => $row->price__number,
            'currency_code' => $row->price__currency_code,
          ],
          'weight' => $row->weight,
        ];
      }
    }
    catch (\Exception $e) {
      // Log the exception.
      $this->loggerChannel->error('Error fetching product options for variation @variation_id with message @message', [
        '@variation_id' => $productVariation->id(),
        '@message' => $e->getMessage(),
      ]);
    }

    return $variationOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function calculatePrice(ProductVariationInterface $productVariation, array $options): Price {
    $price = $productVariation->getPrice();
    $optionsPrice = $this->calculateTotalOptionsPrice($productVariation, $options);

    return $price?->add($optionsPrice);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateTotalOptionsPrice(ProductVariationInterface $productVariation, array $options): Price {
    $optionsPrice = new Price(0, $productVariation->getPrice()->getCurrencyCode());
    foreach ($options as $key => $option) {
      [$string, $optionId] = explode('_', $key);
      $optionPrice = $this->getOptionPrice($productVariation->id(), $optionId, $option);
      if ($optionPrice) {
        $optionsPrice = $optionsPrice?->add($optionPrice);
      }
    }

    return $optionsPrice;
  }

  /**
   * Gets the price for a specific option on a product variation.
   *
   * @param int $variationId
   *   The product variation ID.
   * @param string $optionId
   *   The product option ID.
   * @param string $optionValueId
   *   The product option value ID.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The price.
   */
  private function getOptionPrice(int $variationId, string $optionId, string $optionValueId): ?Price {

    try {
      $record = $this->database->select('commerce_product_variations_product_options', 'cpov')
        ->fields('cpov')
        ->condition('cpov.variation_id', $variationId)
        ->condition('cpov.option_id', $optionId)
        ->condition('cpov.option_value_id', $optionValueId)
        ->execute()
        ->fetch();

      if ($record) {
        return new Price($record->price__number, $record->price__currency_code);
      }
    }
    catch (\Exception $e) {
      // Log the exception.
      $this->loggerChannel->error('Error fetching product option price for variation @variation_id, option @option_id, option value @option_value_id with message @message', [
        '@variation_id' => $variationId,
        '@option_id' => $optionId,
        '@option_value_id' => $optionValueId,
        '@message' => $e->getMessage(),
      ]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLoadedProductOption(string $optionId): ?ProductOptionInterface {
    if (str_contains($optionId, 'option_')) {
      $optionId = str_replace('option_', '', $optionId);
    }

    return $this->productOptionStorage->load($optionId);
  }

  /**
   * {@inheritdoc}
   */
  public function getLoadedProductOptionValue(string $optionValueId): ?ProductOptionValueInterface {
    if (str_contains($optionValueId, 'option_value_')) {
      $optionValueId = str_replace('option_value_', '', $optionValueId);
    }

    /** @var \Drupal\commerce_product_options\Entity\ProductOptionValueInterface $productOptionValue */
    $productOptionValue = $this->productOptionValueStorage->load($optionValueId);
    return $productOptionValue;
  }

}
