The goal of this project is to provide a likewise implementation of Commerce Option in to Drupal Commerce.

Product options are perfectly configurable with Commerce product variations, but if you have a webshop with a lot of options on your product, you have to create hundreds or maybe thousands variations per product. With the current UX of Commerce product variations, this is a lot of work and also not very beneficial for the performance of your database.

This module will try to fill this gap.

## Requirements
This module requires the following modules:

- [Commerce Core](https://www.drupal.org/project/commerce)

## Installation
Download the Commerce Product Options module from the Drupal website or with composer (preferred).
Upload the module to your Drupal site.
Enable the module in the Drupal administration panel.

## Configuration
See the [Configuration instructions](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/commerce-product-options/configuration-instructions) in the [documentation](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/commerce-product-options).

## TODO's
* Automatically create field when enabling product options for a variation type
* Fix @todo's in code
* Write full documentation
* Add composer.json to require min. required PHP version (8.3).

## Troubleshooting
Please create a new issue when you experience an unknown issue.

## Maintainers
- Dieter De Waele ([dtrdewaele](https://www.drupal.org/u/dtrdewaele))
- Rutger Geerolf ([rgeerolf](https://www.drupal.org/u/rgeerolf))

