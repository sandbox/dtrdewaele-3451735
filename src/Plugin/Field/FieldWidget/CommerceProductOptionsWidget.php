<?php

namespace Drupal\commerce_product_options\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\commerce_price\CurrencyFormatter;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product\ProductVariationStorageInterface;
use Drupal\commerce_product_options\Entity\ProductOptionValueInterface;
use Drupal\commerce_product_options\ProductOptionStorageInterface;
use Drupal\commerce_product_options\ProductOptionValueStorageInterface;
use Drupal\commerce_product_options\Service\CommerceProductOptionsServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'commerce_product_options_widget' widget.
 */
#[FieldWidget(
  id: "commerce_product_options_widget",
  label: new TranslatableMarkup("Product options selector"),
  field_types: [
    "commerce_product_options",
  ]
)]
class CommerceProductOptionsWidget extends WidgetBase {

  /**
   * The product option storage.
   *
   * @var \Drupal\commerce_product_options\ProductOptionStorageInterface
   */
  protected ProductOptionStorageInterface $productOptionStorage;

  /**
   * The product option value storage.
   *
   * @var \Drupal\commerce_product_options\ProductOptionValueStorageInterface
   */
  protected ProductOptionValueStorageInterface $productOptionValueStorage;

  /**
   * The product variation storage.
   *
   * @var \Drupal\commerce_product\ProductVariationStorageInterface
   */
  protected ProductVariationStorageInterface $productVariationStorage;

  /**
   * Constructs a new CommerceProductOptionsWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\commerce_product_options\Service\CommerceProductOptionsServiceInterface $commerceProductOptionsService
   *   The commerce product options service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\commerce_price\CurrencyFormatter $currencyFormatter
   *   The currency formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    protected CommerceProductOptionsServiceInterface $commerceProductOptionsService,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected CurrencyFormatter $currencyFormatter,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    /** @var \Drupal\commerce_product_options\ProductOptionStorageInterface $productOptionStorage */
    $productOptionStorage = $this->entityTypeManager->getStorage('commerce_product_option');
    $this->productOptionStorage = $productOptionStorage;

    /** @var \Drupal\commerce_product_options\ProductOptionValueStorageInterface $productOptionValueStorage */
    $productOptionValueStorage = $this->entityTypeManager->getStorage('commerce_product_option_value');
    $this->productOptionValueStorage = $productOptionValueStorage;

    /** @var \Drupal\commerce_product\ProductVariationStorageInterface $productVariationStorage */
    $productVariationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');
    $this->productVariationStorage = $productVariationStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): CommerceProductOptionsWidget {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('commerce_product_options.service'),
      $container->get('entity_type.manager'),
      $container->get('commerce_price.currency_formatter'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $purchasedEntity = $items->getParent()?->getEntity()->getPurchasedEntity();
    if (!$purchasedEntity instanceof ProductVariationInterface) {
      return [];
    }

    $productOptions = $this->commerceProductOptionsService->getVariationOptions($purchasedEntity);

    $fieldset = [
      '#type' => 'fieldset',
      '#title' => $this->t('Product options'),
      '#attributes' => [
        'class' => ['product-options-wrapper'],
      ],
    ];

    foreach ($productOptions as $key => $productOption) {
      $commerceProductOption = $this->commerceProductOptionsService->getLoadedProductOption($key);

      if ($commerceProductOption) {
        $fieldset[$key] = [
          '#type' => 'select',
          '#title' => $commerceProductOption->label(),
          '#options' => $this->buildProductOptionValuesArray($productOption['options']),
          '#empty_option' => '- ' . $this->t('Make your choice') . ' -',
          // '#default_value' => NULL,
          // '#required' => $optionData['required'],
          '#ajax' => [
            'callback' => [$this, 'ajaxRefresh'],
            'wrapper' => 'product-options-wrapper',
            'effect' => 'fade',
          ],
        ];
      }
    }

    return ['value' => $fieldset];
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultSettings() {
    return [
      'price_field_selector' => '.field--name-price',
      'list_price_field_selector' => '.field--name-list-price',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['price_field_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price field selector'),
      '#description' => $this->t('The selector of the price field. Change this if your theme uses a different selector than the default price field.'),
      '#default_value' => $this->getSetting('price_field_selector'),
      '#required' => TRUE,
    ];

    $element['list_price_field_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List price field selector'),
      '#description' => $this->t('The selector of the list price field. Change this if your theme uses a different selector than the default list price field.'),
      '#default_value' => $this->getSetting('list_price_field_selector'),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Price field selector: @selector', ['@selector' => $this->getSetting('price_field_selector')]);
    $summary[] = $this->t('List price field selector: @selector', ['@selector' => $this->getSetting('list_price_field_selector')]);

    return $summary;
  }

  /**
   * Build the product option values array to use in a selectbox.
   *
   * @param array $productOptionValueData
   *   The product option value data.
   *
   * @return array
   *   The product option values array.
   */
  protected function buildProductOptionValuesArray(array $productOptionValueData): array {
    $optionValues = [];
    foreach ($productOptionValueData as $productOptionValue) {
      $productOptionValueEntity = $this->productOptionValueStorage->load($productOptionValue['value']);
      if ($productOptionValueEntity instanceof ProductOptionValueInterface) {
        $formattedPrice = $this->currencyFormatter->format($productOptionValue['price']['number'], $productOptionValue['price']['currency_code']);
        $optionLabel = $productOptionValueEntity->label() . ' (' . $formattedPrice . ')';
        $optionValues[$productOptionValueEntity->id()] = $optionLabel;
      }
    }

    return $optionValues;
  }

  /**
   * Ajax callback for refreshing the price after you have selected an option.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   *
   * @throws \Exception
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();

    $fieldName = $this->fieldDefinition->getName();
    $purchasedEntity = $form_state->getValue([
      'purchased_entity', 0, 'variation',
    ]);
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $purchasedEntity */
    $purchasedEntity = $this->productVariationStorage->load($purchasedEntity);
    if ($purchasedEntity) {
      $productOptions = $form_state->getValue([$fieldName, 0, 'value']);
      $productOptions = array_filter($productOptions);

      // Handle the normal price.
      $optionsPrice = $this->commerceProductOptionsService->calculateTotalOptionsPrice(
        $purchasedEntity,
        $productOptions
      );

      $purchasedEntity->setPrice($purchasedEntity->getPrice()?->add($optionsPrice));
      $renderedPrice = $purchasedEntity->get('price')->view('default');
      $priceField = $this->renderer->render($renderedPrice);

      // @todo make selector configurable in the widget settings.
      $response->addCommand(new ReplaceCommand(
          $this->getSetting('price_field_selector'),
          $priceField)
      );

      if (
        $purchasedEntity->hasField('list_price')
        && !$purchasedEntity->get('list_price')->isEmpty()
      ) {
        $purchasedEntity->setListPrice($purchasedEntity->getListPrice()->add($optionsPrice));
        $renderedListPrice = $purchasedEntity->get('list_price')->view('default');
        $listPriceField = $this->renderer->render($renderedListPrice);
        // @todo make selector configurable in the widget settings.
        $response->addCommand(new ReplaceCommand(
            $this->getSetting('list_price_field_selector'),
            $listPriceField)
        );
      }
    }

    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): string {
    $values = parent::massageFormValues($values, $form, $form_state);
    return serialize($values[0]['value']);
  }

}
