<?php

namespace Drupal\commerce_product_options\Service;

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_options\Entity\ProductOptionInterface;
use Drupal\commerce_product_options\Entity\ProductOptionValueInterface;

/**
 * Interface for the commerce product options service.
 */
interface CommerceProductOptionsServiceInterface {

  /**
   * Gets the options for the given product variation.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation
   *   The product variation.
   *
   * @return array
   *   The options.
   */
  public function getVariationOptions(ProductVariationInterface $productVariation): array;

  /**
   * Calculates the price of the given product variation with the given options.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation
   *   The product variation.
   * @param array $options
   *   The options.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public function calculatePrice(ProductVariationInterface $productVariation, array $options): Price;

  /**
   * Calculates the total price of the chosen options
   * of the given product variation.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation
   *   The product variation.
   * @param array $options
   *   The options.
   *
   * @return \Drupal\commerce_price\Price
   *  The price of the chosen options combined.
   */
  public function calculateTotalOptionsPrice(ProductVariationInterface $productVariation, array $options): Price;

  /**
   * Gets the loaded product option based on the given option ID.
   *
   * @param string $optionId
   *   The option ID.
   *
   * @return \Drupal\commerce_product_options\Entity\ProductOptionInterface|null
   */
  public function getLoadedProductOption(string $optionId): ?ProductOptionInterface;

  /**
   * Gets the loaded product option value based on the given option value ID.
   *
   * @param string $optionValueId
   *   The option value ID.
   *
   * @return \Drupal\commerce_product_options\Entity\ProductOptionValueInterface|null
   */
  public function getLoadedProductOptionValue(string $optionValueId): ?ProductOptionValueInterface;

}
