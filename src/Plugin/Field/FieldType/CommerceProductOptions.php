<?php

namespace Drupal\commerce_product_options\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'commerce_product_options' field type.
 */
#[FieldType(
  id: "commerce_product_options",
  label: new TranslatableMarkup("Product options"),
  description: new TranslatableMarkup("Stores a decimal number and a three letter currency code."),
  category: "commerce",
  default_widget: "commerce_product_options_widget",
  default_formatter: "commerce_product_options_formatter",
)]
class CommerceProductOptions extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('string')->setLabel(t('Serialized value'));
    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'size' => 'normal',
        ],
      ],
    ];
  }

}
