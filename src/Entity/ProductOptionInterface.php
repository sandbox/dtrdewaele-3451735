<?php

declare(strict_types=1);

namespace Drupal\commerce_product_options\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a product option entity type.
 */
interface ProductOptionInterface extends ConfigEntityInterface {

  /**
   * Get all product option values for a product option.
   *
   * @return array
   *   An array of product option values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getValues(): array;

  /**
   * Load the product option entity.
   *
   * @return \Drupal\commerce_product_options\Entity\ProductOptionInterface
   *   The product option entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOption(): ProductOptionInterface;

  /**
   * Get the product variation types.
   *
   * @return array
   *   An array of product variation types.
   */
  public function getVariationTypes(): array;

}
