<?php

namespace Drupal\commerce_product_options;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the product option value storage.
 */
class ProductOptionValueStorage extends SqlContentEntityStorage implements ProductOptionValueStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByOption($optionId): array {
    $query = $this->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('option', $optionId);
    $query->sort('name');
    $result = $query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

}
