<?php

declare(strict_types=1);

namespace Drupal\commerce_product_options\Form;

use Drupal\commerce\EntityHelper;
use Drupal\commerce\InlineFormManager;
use Drupal\commerce_product\Form\ProductAttributeForm;
use Drupal\commerce_product\ProductAttributeFieldManagerInterface;
use Drupal\commerce_product_options\Entity\ProductOption;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Product option form.
 *
 * // @todo get rid of ProductAttributeForm dependency.
 */
final class ProductOptionForm extends ProductAttributeForm {

  public function __construct(
    ProductAttributeFieldManagerInterface $attribute_field_manager,
    InlineFormManager $inline_form_manager,
    protected CacheBackendInterface $cache,
  ) {
    parent::__construct($attribute_field_manager, $inline_form_manager);
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_product.attribute_field_manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('cache.discovery')
    );
  }

  /**
   * Build the form for the product option.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function form(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\commerce_product_options\Entity\ProductOptionInterface $productOption */
    $productOption = $this->entity;
    $form = BundleEntityFormBase::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $productOption->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $productOption->id(),
      '#machine_name' => [
        'exists' => [ProductOption::class, 'load'],
      ],
      '#disabled' => !$productOption->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $productOption->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $productOption->get('description'),
    ];

    $variationTypeStorage = $this->entityTypeManager->getStorage('commerce_product_variation_type');
    $variationTypes = $variationTypeStorage->loadMultiple();

    $form['product_variation_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Product variation types'),
      '#description' => $this->t('Select the variation types where you want to be able to select this option.'),
      '#required' => TRUE,
      '#default_value' => $productOption->get('product_variation_types') ?? [],
    ];

    foreach ($variationTypes as $variationType) {
      $form['product_variation_types']['#options'][$variationType->id()] = $variationType->label();
    }

    // Product option values can only be created if the option is not new.
    if (!$this->entity->isNew()) {
      $form = $this->buildValuesForm($form, $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = $this->entity->save();

    if ($status === SAVED_NEW) {
      // @todo: clear specific cache.
      $this->cache->deleteAll();
      $this->messenger()->addMessage($this->t('Created the %label product option.', ['%label' => $this->entity->label()]));
      // Send the user to the edit form to create the attribute values.
      $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
    }
    else {
      try {
        $this->saveOptionValues($form, $form_state);
      }
      catch (\Exception $e) {
        $this->messenger()->addError($this->t(
          'An error occurred while saving the product option values with @message.', [
            '@message' => $e->getMessage(),
          ]));
      }

      $this->saveValues($form, $form_state);
      $this->messenger()->addMessage($this->t('Updated the %label product option.', ['%label' => $this->entity->label()]));
      $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    }

    return $status;
  }

  /**
   * Builds the form for managing the option values.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildValuesForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\commerce_product_options\Entity\ProductOptionInterface $option */
    $option = $this->entity;
    $values = $option->getValues();
    $user_input = $form_state->getUserInput();
    // Reorder the values by name, if requested.
    if ($form_state->get('reset_alphabetical')) {
      $value_names = EntityHelper::extractLabels($values);
      asort($value_names);
      foreach (array_keys($value_names) as $weight => $id) {
        $values[$id]->setWeight((int) $weight);
      }
    }
    // The value map allows new values to be added and removed before saving.
    // An array in the $index => $id format. $id is '_new' for unsaved values.
    $value_map = (array) $form_state->get('value_map');
    if (empty($value_map)) {
      $value_map = $values ? array_keys($values) : ['_new'];
      $form_state->set('value_map', $value_map);
    }

    $wrapper_id = Html::getUniqueId('product-option-values-ajax-wrapper');
    $form['values'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Value'), 'colspan' => 2],
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'product-option-value-order-weight',
        ],
      ],
      '#weight' => 5,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      // #input defaults to TRUE, which breaks file fields on the value form.
      // This table is used for visual grouping only, the element itself
      // doesn't have any values of its own that need processing.
      '#input' => FALSE,
    ];
    // Make the weight list always reflect the current number of values.
    // Taken from WidgetBase::formMultipleElements().
    $max_weight = count($value_map);

    foreach ($value_map as $index => $id) {
      $value_form = &$form['values'][$index];
      // The tabledrag element is always added to the first cell in the row,
      // so we add an empty cell to guide it there, for better styling.
      $value_form['#attributes']['class'][] = 'draggable';
      $value_form['tabledrag'] = [
        '#markup' => '',
      ];
      if ($id === '_new') {
        $value = $this->entityTypeManager->getStorage('commerce_product_option_value')
          ->create([
            'option' => $option->id(),
            'langcode' => $option->get('langcode'),
          ]);
        $default_weight = $max_weight;
        $remove_access = TRUE;
      }
      else {
        $value = $values[$id];
        $default_weight = $value->getWeight();
        $remove_access = $value->access('delete');
      }
      $inline_form = $this->inlineFormManager->createInstance('content_entity', [
        'skip_save' => TRUE,
      ], $value);

      $value_form['entity'] = [
        '#parents' => ['values', $index, 'entity'],
        '#inline_form' => $inline_form,
      ];
      $value_form['entity'] = $inline_form->buildInlineForm($value_form['entity'], $form_state);

      $value_form['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#delta' => $max_weight,
        '#default_value' => $default_weight,
        '#attributes' => [
          'class' => ['product-option-value-order-weight'],
        ],
      ];
      // Used by SortArray::sortByWeightProperty to sort the rows.
      if (isset($user_input['values'][$index])) {
        $input_weight = $user_input['values'][$index]['weight'];
        // If the weights were just reset, reflect it in the user input.
        if ($form_state->get('reset_alphabetical')) {
          $input_weight = $default_weight;
        }
        // Make sure the weight is not out of bounds due to removals.
        if ($user_input['values'][$index]['weight'] > $max_weight) {
          $input_weight = $max_weight;
        }
        // Reflect the updated user input on the element.
        $value_form['weight']['#value'] = $input_weight;

        $value_form['#weight'] = $input_weight;
      }
      else {
        $value_form['#weight'] = $default_weight;
      }

      $value_form['remove'] = [
        '#type' => 'submit',
        '#name' => 'remove_value' . $index,
        '#value' => $this->t('Remove'),
        '#limit_validation_errors' => [],
        '#submit' => ['::removeValueSubmit'],
        '#value_index' => $index,
        '#ajax' => [
          'callback' => '::valuesAjax',
          'wrapper' => $wrapper_id,
        ],
        '#access' => $remove_access,
      ];
    }

    // Sort the values by weight. Ensures weight is preserved on ajax refresh.
    uasort($form['values'], [
      SortArray::class,
      'sortByWeightProperty',
    ]);

    $access_handler = $this->entityTypeManager->getAccessControlHandler('commerce_product_attribute_value');
    if ($access_handler->createAccess($option->id())) {
      $form['values']['_add_new'] = [
        '#tree' => FALSE,
      ];
      $form['values']['_add_new']['entity'] = [
        '#type' => 'container',
        '#wrapper_attributes' => ['colspan' => 2],
      ];
      $form['values']['_add_new']['entity']['add_value'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add value'),
        '#submit' => ['::addValueSubmit'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::valuesAjax',
          'wrapper' => $wrapper_id,
        ],
      ];
      $form['values']['_add_new']['entity']['reset_alphabetical'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset to alphabetical'),
        '#submit' => ['::resetAlphabeticalSubmit'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::valuesAjax',
          'wrapper' => $wrapper_id,
        ],
      ];
      $form['values']['_add_new']['operations'] = [
        'data' => [],
      ];
    }

    return $form;
  }

  /**
   * Saves the option values from the form state.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function saveOptionValues(array $form, FormStateInterface $form_state): void {
    $delete_queue = $form_state->get('delete_queue');
    if (!empty($delete_queue)) {
      $value_storage = $this->entityTypeManager->getStorage('commerce_product_option_value');
      $values = $value_storage->loadMultiple($delete_queue);
      $value_storage->delete($values);
    }

    foreach ($form_state->getValue(['values']) as $index => $valueData) {
      $inlineForm = $form['values'][$index]['entity']['#inline_form'];
      $value = $inlineForm->getEntity();
      $value->setWeight((int) $valueData['weight']);
      $value->save();
    }
  }

}
