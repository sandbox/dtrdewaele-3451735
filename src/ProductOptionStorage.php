<?php

namespace Drupal\commerce_product_options;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Defines the product option storage.
 */
class ProductOptionStorage extends ConfigEntityStorage implements ProductOptionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByVariationType(string $variationTypeId): array {
    $query = $this->getQuery();
    $query->accessCheck(FALSE);
    $result = $query->execute();
    $options = $result ? $this->loadMultiple($result) : [];

    /**
     * @var int $key
     * @var \Drupal\commerce_product_options\Entity\ProductOptionInterface $option
     */
    foreach ($options as $key => $option) {
      $variationTypes = $option->getVariationTypes();
      if (!in_array($variationTypeId, $variationTypes, TRUE)) {
        unset($options[$key]);
      }
    }

    return $options;
  }

}
