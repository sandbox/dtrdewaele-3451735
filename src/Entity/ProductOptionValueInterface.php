<?php

declare(strict_types=1);

namespace Drupal\commerce_product_options\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a product option value entity type.
 */
interface ProductOptionValueInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Get the product option value weight.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int;

  /**
   * Set the product option value weight.
   *
   * @param int $weight
   *   The weight.
   */
  public function setWeight(int $weight): void;

}
